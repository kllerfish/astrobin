# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-05-29 10:53+0000\n"
"PO-Revision-Date: 2013-11-03 12:59+0100\n"
"Last-Translator: Salvatore <salvatore@iovene.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.1.6\n"

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:10
msgid "Getting plate-solving status, please wait..."
msgstr "Controllo dello stato della riduzione astrometrica, attendere..."

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:28
msgid "Plate-solving process starting, please do not leave this page..."
msgstr ""
"Riduzione astrometrica in fase di iniziazione, non chiudere questa pagina..."

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:29
msgid "Plate-solving started in the background. You may leave this page."
msgstr "Riduzione astrometrica in corso. Puoi chiudere questa pagina."

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:30
msgid "Plate-solving almost ready, please do not leave this page..."
msgstr "Riduzione astrmetrica quasi pronta, non chiudere questa pagina..."

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:31
msgid "This image could not be plate-solved."
msgstr "Non è stato possibile ridurre l'immagine astrometricamente."

#: templates/astrobin_apps_platesolving/inclusion_tags/platesolving_machinery.html:32
msgid ""
"This image has been successfully plate-solved. Please refresh the page to "
"see the new data."
msgstr ""
"L'immagine è stata ridotta astrometricamente. Ricarica la pagine per vedere "
"i nuovi dati."
